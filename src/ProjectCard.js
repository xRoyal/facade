import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Icon from "@mdi/react";

const useStyles = makeStyles({
  project: {
    width: "100%",
    border: "1px solid #cecece",
    borderRadius: "5px",
    display: "grid",
    gridTemplateColumns: "70px auto",
    paddingBottom: 10,
  },
  iconBack: {
    borderRadius: "10px",
    width: 50,
    height: 50,
    display: "flex",
    alignItems: "center",
    margin: 10,
  },
  icon: {
    margin: "0 auto",
  },
  textBlocks: {
    textAlign: "left",
  },
  progressOuter: {
    border: "1px solid #f2f2f2",
    borderRadius: "2px",
    height: "100%",
    overflow: "hidden",
  },
  progressInner: {
    height: "100%",
  },
  progress: {
    height: "15px",
    marginTop: "5px",
    display: "grid",
    gridTemplateColumns: "85% 15%",
  },
  contentBlock: {
    width: "80%",
    margin: 10,
  },
  captionText: {
    color: "#333333",
  },
  percentLabel: {
    color: "#696969",
    marginLeft: 5,
    height: "15px",
    fontSize: "0.8rem",
  },
});

const ProjectCard = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.project}>
      <div>
        <div
          className={classes.iconBack}
          style={{ background: props.gradient }}
        >
          <Icon
            className={classes.icon}
            path={props.icon}
            size={1.2}
            color="white"
          />
        </div>
      </div>

      <div className={classes.contentBlock}>
        <div className={classes.textBlocks}>
          <Typography variant="body1">{props.name}</Typography>
          <Typography variant="caption" className={classes.captionText}>
            {props.description}
          </Typography>
        </div>
        <div className={classes.progress}>
          <div className={classes.progressOuter}>
            <div
              className={classes.progressInner}
              style={{ width: props.percent || 0, background: props.gradient }}
            ></div>
          </div>
          <Typography className={classes.percentLabel}>
            {props.percent}
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default ProjectCard;
