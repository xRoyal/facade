import React from "react";
import "./App.css";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ProjectCard from "./ProjectCard";
import { mdiEthereum, mdiPin, mdiCodeArray, mdiGamepad } from "@mdi/js";

const useStyles = makeStyles({
  projects: {
    width: "100%",
    margin: "30px auto",
    display: "grid",
    gridTemplateColumns: "repeat(1, 1fr)",
    //eslint-disable-next-line
    ["@media (min-width:750px)"]: {
      width: "100%",
      gridTemplateColumns: "repeat(2, 1fr)",
    },
    //eslint-disable-next-line
    ["@media (min-width:1140px)"]: {
      width: "80%",
      gridTemplateColumns: "repeat(3, 1fr)",
    },

    gridGap: "20px",
    gridAutoRows: "120px",
  },
  projectsWrapper: {
    width: "80%",
    borderTop: "1px solid #cecece",
    margin: "0px auto",
    paddingTop: "40px",
  },
  projectsHeader: {
    textAlign: "center",
  },
  text: {
    marginBottom: "20px",
  },
  nameTitle: {
    maxWidth: "90%",
    margin: "0 auto",
  },
});

const App = () => {
  const classes = useStyles();

  const data = [
    {
      name: "This site",
      description: "A simple landing page with project tracking",
      gradient: "linear-gradient(120deg, #89f7fe 0%, #66a6ff 100%)",
      icon: mdiPin,
      percent: "100%",
    },
    {
      name: "Fuzzing paper",
      description: "My honours project, modifying AFL fuzzing",
      gradient: "linear-gradient(to right, #f12711, #f5af19)",
      icon: mdiCodeArray,
      percent: "55%",
    },
    {
      name: "Paint",
      description: "A 3D puzzle game made in godot",
      gradient: "linear-gradient(315deg, #00b712 0%, #5aff15 74%)",
      icon: mdiGamepad,
      percent: "30%",
    },
    {
      name: "Royal",
      description: "Cryptocurreny trading with machine learning",
      gradient: "linear-gradient(315deg, #fbb034 0%, #ffdd00 74%)",
      icon: mdiEthereum,
      percent: "10%",
    },
  ];

  return (
    <div className="App">
      <img src="/n.png" width="auto" height="150" alt="crown"></img>
      <div className={classes.text}>
        <Typography variant="h3" className={classes.nameTitle}>
          Aidan Sawers
        </Typography>
        <Typography variant="h6">aidan@sawers.dev</Typography>
      </div>
      <div className={classes.projectsWrapper}>
        <Typography variant="h4" className={classes.projectsHeader}>
          Projects
        </Typography>
        <div className={classes.projects}>
          {data.map((item) => {
            return (
              <ProjectCard
                name={item.name}
                description={item.description}
                gradient={item.gradient}
                icon={item.icon}
                percent={item.percent}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default App;
